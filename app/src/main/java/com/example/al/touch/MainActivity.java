package com.example.al.touch;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends Activity implements View.OnTouchListener {

    final static int STATE_STAY = 0;
    final static int STATE_MOVE = 1;
    ImageView img;
    int state = STATE_STAY;
    int left = 0;
    int top = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View root = findViewById(android.R.id.content).getRootView();
        root.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("_TOUCH_ROOT_", event.toString());
                if (STATE_MOVE == state) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            img.setX((int)event.getX() - left);
                            img.setY((int)event.getY() - top);
                            break;
                        case MotionEvent.ACTION_UP:
                            state = STATE_STAY;
                            break;
                    }
                }
                return false;
            }
        });

        img = (ImageView)findViewById(R.id.img);
        img.setOnTouchListener(this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.i("_TOUCH_IMG_", event.toString());
        state = STATE_MOVE;
        left = (int)event.getX();
        top = (int)event.getY();
        return false;
    }
}
